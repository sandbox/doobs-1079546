<?php

function proposaldb_initialisedb_mailhosts() {
  db_query('DELETE FROM {proposaldb_mailhosts} where h_id > 0'); // purge table

// check /etc/services for default port allocations
  db_query("INSERT INTO {proposaldb_mailhosts} (h_id, active, host_name, protocol, port, security)"
           ." VALUES (%d, %d, '%s', '%s', %d, '%s') ", 1, 1, 'pop.server.example.org', 'SMTPS',465,'SSL'); 

/*
  db_query("INSERT INTO {proposaldb_mailhosts} (hid, host_name, protocol, port, security)"
           ." VALUES (%d, '%s', '%s', %d, '%s') ", 1, 'pop.server.example.org', 'IMAPS',993,'SSL');  
  db_query("INSERT INTO {proposaldb_mailhosts} (hid, host_name, protocol, port, security)"
           ." VALUES (%d, '%s', '%s', %d, '%s') ", 1, 'pop.server.example.org', 'POP3S',995,'SSL');  
 */
/*
   drupal auth module adds  ssl:// or tls:// to the host.address to connect as an SSL or TLS client
   That assumes that SSL support was built into your version of PHP

   the default assumes tcp://
   we don't cater for STARTLS command on SMTP or POP3 or IMAP

   
 */

}
