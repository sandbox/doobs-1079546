<?php

$machine_header = array(
   'm_id'       =>"%d", 
   'active'     =>"%d", 
   'analytical' =>"%d", 
   'short_name' =>"'%s'", 
   'full_name'  =>"'%s'", 
   'description'=>"'%s'",
);

$field_header = array(
   'f_id'        =>"%d", 
   'name'        =>"'%s'",
   'title'       =>"'%s'",
   'tabtitle'    =>"'%s'",
   'description' =>"'%s'",
   'deflt'       =>"'%s'",
   'type'        =>"'%s'",
   'units'       =>"'%s'",
   'format'      =>"'%s'",
   'widget'      =>"'%s'",
   'displaysize' =>"%d",
   'maxsize'     =>"%d",
   'weight'      =>"%d",
);

$fieldmap_header = array(
   'm_id'        =>'%d', 
   'i_id'        =>'%d',
   'f_id'        =>'%d',
   'mandatory'   =>'%d',
   'tabular'     =>'%d',
   'weight'      =>'%d',
   'class'       =>"'%s'",
);

$machine_data = array(
   'm_id'       =>NULL, 
   'active'     =>1, 
   'analytical' =>1, 
   'short_name' =>'FR591', 
   'full_name'  =>'Bruker FR591 Kappa Apex II', 
   'description'=> t("Bruker FR591 generator, Mo rotating anode and Kappa goniometer with Apex II CCD detector"), 
);
$m1=proposaldb_machine_dbinit_record($machine_header,$machine_data);

$machine_data = array(
   'm_id'       =>NULL, 
   'active'     =>1, 
   'analytical' =>1, 
   'short_name' =>'SMART', 
   'full_name'  =>'Bruker SMART 1000', 
   'description'=> t("Bruker SMART 1000, Mo sealed tube, fixed Chi goniometer with CCD detector"),
);
$m2=proposaldb_machine_dbinit_record($machine_header,$machine_data);


$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'kv',
   'title'       =>'Generator voltage',
   'tabtitle'    =>'kV',
   'description' =>' ',
   'deflt'       =>'50',
   'type'        =>'float',
   'units'       =>'kV',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>100,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);




$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'ma',
   'title'       =>'Generator current',
   'tabtitle'    =>'mA',
   'description' =>' ',
   'deflt'       =>'20',
   'type'        =>'float',
   'units'       =>'mA',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>105,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'pressure',
   'title'       =>'X-ray tube pressure',
   'tabtitle'    =>'Vacuum',
   'description' =>' ',
   'deflt'       =>'1.0E-7',
   'type'        =>'float',
   'units'       =>'Atm.',
   'format'      =>'%.2e',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>110,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);




$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'temp_ccd',
   'title'       =>'CCD temperature',
   'tabtitle'    =>'CCD (C)',
   'description' =>' ',
   'deflt'       =>'-60',
   'type'        =>'float',
   'units'       =>'°C',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>115,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'bias',
   'title'       =>'Detector bias',
   'tabtitle'    =>'Bias',
   'description' =>' ',
   'deflt'       =>'250',
   'type'        =>'float',
   'units'       =>'',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>135,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);

proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'backplate',
   'title'       =>'Detector backplate position',
   'tabtitle'    =>'backplate',
   'description' =>' ',
   'deflt'       =>'10.0',
   'type'        =>'float',
   'units'       =>'',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>140,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'instrument',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'temp_sample',
   'title'       =>'Sample temperature',
   'tabtitle'    =>'Sample (K)',
   'description' =>' ',
   'deflt'       =>'150',
   'type'        =>'float',
   'units'       =>'K',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>155,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'analytic',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'analytic',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'exposuretime',
   'title'       =>'Exposure time/frame',
   'tabtitle'    =>'Exposure time (s)',
   'description' =>' ',
   'deflt'       =>'10',
   'type'        =>'float',
   'units'       =>'s',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>165,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'analytic',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'analytic',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'frames',
   'title'       =>'Total number of frames',
   'tabtitle'    =>'Frames',
   'description' =>' ',
   'deflt'       =>'',
   'type'        =>'int',
   'units'       =>'',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>175,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'analytic',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>1,
   'weight'      =>1,
   'class'       =>'analytic',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);


// ========================================================================


$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'colour',
   'title'       =>'Colour',
   'tabtitle'    =>'Colour',
   'description' =>'Specimen colour',
   'deflt'       =>'',
   'type'        =>'text',
   'units'       =>'',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>20,
   'maxsize'     =>80,
   'weight'      =>185,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'habit',
   'title'       =>'Habit',
   'tabtitle'    =>'Habit',
   'description' =>'Crystal habit of the specimen?',
   'deflt'       =>'',
   'type'        =>'text',
   'units'       =>'',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>20,
   'maxsize'     =>80,
   'weight'      =>190,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'min',
   'title'       =>'Min',
   'tabtitle'    =>'Min',
   'description' =>'Minimum dimension colour',
   'deflt'       =>'',
   'type'        =>'float',
   'units'       =>'µm',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>195,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);


$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'mid',
   'title'       =>'Min',
   'tabtitle'    =>'Min',
   'description' =>'Middle dimension',
   'deflt'       =>'',
   'type'        =>'float',
   'units'       =>'µm',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>200,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);


$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'max',
   'title'       =>'Max',
   'tabtitle'    =>'Max',
   'description' =>'Maximum crystal dimension',
   'deflt'       =>'',
   'type'        =>'float',
   'units'       =>'µm',
   'format'      =>'',
   'widget'      =>'textfield',
   'displaysize' =>10,
   'maxsize'     =>10,
   'weight'      =>205,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>1,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);



$field_data = array(
   'f_id'        =>NULL, 
   'name'        =>'notes',
   'title'       =>'Notes',
   'tabtitle'    =>'Notes',
   'description' =>'Anything unusual about the sample studied?',
   'deflt'       =>'',
   'type'        =>'text',
   'units'       =>'',
   'format'      =>'',
   'widget'      =>'textarea',
   'displaysize' =>20,
   'maxsize'     =>2000,
   'weight'      =>210,
);
$f_id=proposaldb_log_fields_dbinit_record($field_header,$field_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m1,
   'f_id'        =>$f_id,
   'mandatory'   =>0,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

$fieldmap_data = array(
   'm_id'        =>NULL, 
   'i_id'        =>$m2,
   'f_id'        =>$f_id,
   'mandatory'   =>0,
   'tabular'     =>0,
   'weight'      =>1,
   'class'       =>'sample',
);
proposaldb_log_mach_field_map_dbinit_record($fieldmap_header,$fieldmap_data);

